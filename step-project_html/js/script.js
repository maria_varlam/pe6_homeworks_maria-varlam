"use strict";

const content = document.getElementsByClassName("services_content");
const clickHandler = (event)=>{
const clickedLi = event.target;
const active = document.getElementsByClassName("active");
if (active.length>0) active[0].classList.remove("active")
clickedLi.classList.add("active");
for (let el of content) {
   el.classList.add("hidden");
   if (el.dataset.tab === clickedLi.dataset.tab){
       el.classList.remove("hidden")
   }
}
}
const tabs = document.getElementById("tabs");
tabs.addEventListener("click", clickHandler);

const filterItems= document.querySelectorAll(".work_examples");
const workFilter = document.querySelector(".work_list");
workFilter.addEventListener("click", (event)=>{
    let filterClass = event.target.dataset['filter'];
    filterItems.forEach((elem)=>{
        elem.classList.remove("hide");
        if (!elem.classList.contains(filterClass) && filterClass!=='all') {
            elem.classList.add("hide")
        }
    })
})
const loadBtn = document.querySelector(".load_button");
loadBtn.addEventListener("click",()=>{
    filterItems.forEach((el)=>{
        el.classList.remove("hidden")
    })
    loadBtn.classList.add("hidden")
})

const tabFeedback = document.getElementsByClassName("feedback_slide");
const slider = document.getElementById("slider");
const iconHandler = (event)=>{
    const clickedIcon = event.target;
    const shown = document.getElementsByClassName("shown");
    if (shown.length>0) shown[0].classList.remove("shown");
    clickedIcon.classList.add("shown");
    for (let el of tabFeedback) {
        el.classList.add("hidden");
        if (el.dataset.name === clickedIcon.dataset.name){
            el.classList.remove("hidden")
        }
    }
}
slider.addEventListener("click", iconHandler);


const iconsWrapper = document.querySelector("#slider");
const btnNextSlide = document.querySelector("#btn_next");
const icon = document.querySelectorAll(".feedback_slider_image");

const slideText = document.querySelectorAll(".feedback_slide");
let activeIndex = 0;
btnNextSlide.addEventListener("click",()=>{
    icon.forEach((element,index)=> {
        if (element.classList.contains("shown")) {
          if (index === icon.length - 1) {
            activeIndex = 0;
          } else {
            activeIndex = index + 1;
          }
        }
        element.classList.remove("shown");
    });
    icon[activeIndex].classList.add("shown");

    // slideText.forEach((item)=>{
    //     icon.forEach((item)=>{
    //         console.log(item)
    //     })
        // if (item.dataset.name===) {
            
        // }
    })
        // if (icon[activeIndex].classList.contains("shown") && icon[activeIndex].dataset.name===slideText.dataset.name) {
        //     document.querySelector(".feedback_slide").classList.add("hidden");
        //     const shown = document.getElementsByClassName("shown");
        //     if (shown.length>0) shown[0].classList.remove("shown");
        // }
    //     if(element.dataset.name===document.querySelector(".feedback_slide").dataset.name){
    //         for (let el of tabFeedback) {
    //             el.classList.add("hidden");
    //             const shown = document.getElementsByClassName("shown");
    //         if (shown.length>0) shown[0].classList.remove("shown");
    //     }
    //   }
    //   icon[activeIndex].classList.add("shown");
   
    // })
// })