"use strict";

// const user = {
//     name: "Uasya",
//     age:33,
//     getName(){
//         return this.name;
//     },
//     getAge: function(){
//         return ()=>{this.age};
//     }
// }
// console.log(this)
// console.log(user.getName())
// console.log(user.getAge())

// Каррирование:
// function summ(a,b,c) {
// return a+b+c    
// }
// function summKar(a) {
//     return function(b){
//         return function(c){
//             return a+b+c;
//         }
//     }   
// }
// const res = summKar(1)(2)(3)
// console.log(res)
// const res = summKar(1)
// console.log(res(2)(3))


// function createUser (){
//     return {
//             name: "Uasya",
//             age:33,
//             getName(){
//                 return this.name;
//             },
//             getAge: function(){
//                 return ()=>{this.age};
//             }
//         }
//     }
// console.log(createUser())

// console.log(new Number(12)) //фун конструктор

// function CreateUser(name, age){ //получается универсальная функция при добавлении параметров в функцию
//     this.name = name,
//     this.age = age;
//     this.getName= function(){
//         return this.name;
//     },
//     this.getAge= function(){
//         return this.age;
//     }
// }
// const user = new CreateUser("Uasya", 18)
// console.log(user)
// Object.defineProperty(this, "age", {
//     set: function(value){ //замена свойств значения 
//         if (value<=20){
//             this._age = value;
//         }
//     },
//     get: function(){  
//         return this._age +1;
//     }
// })

// user.age = 22;
// console.log(user.age)
// const user2 = new CreateUser("Ndi", 33)
// console.log(user2)

// const user3 = user;
// user3.name="JJJ";
// console.log(user3)
// console.log(user) //переписывает и юзер тоже 

//bind call apply - три метода фун и помогают изменить this. Первый элемент - объект на который ссылается this
// const user = {
//     name: "Uasya",
//     age:33,
//     getName(){
//         return this.name;
//     },
//     getAge: function(){
//         return ()=>{return this.age};
//     }
// }
// console.log(user.getName())
// const user2 = {name: "Stepan"}
// user2.getName = user.getName.bind(user2)
// // console.log(user.getName.bind(user2)) //меняет ссылку в this
// console.log(user2.getName())
// console.log(user.getName.bind(user2)())
// console.log(user.getName.call(user2))
// console.log(user.getName.apply(user2))



// ##TASK 1
// Напишите функцию-конструктор, создающую объект, реализующий такой
// функционал: у нас на странице есть вопрос. При первом клике на него под ним
// открывается ответ на вопрос. При повтором - ответ прячется. Разметку вы
// найдете в файле.
//  <a href="" class="question">Девиз дома Баратеонов</a>
//  <div id="root"></div>
// const questionText = "Девиз дома Баратеонов"; const questionAnswer = "Нам
// ярость!";

// const questionText = "Девиз дома Баратеонов"; 
// const questionAnswer = "Нам ярость!";

// const root = document.querySelector("#root");

// function CreateQuestion(question, answer){
//     this.question = question;
//     this.answer = answer;

//     this.renderQuestion = function(){
//         const tagA = document.createElement("a");
//         const tagP = document.createElement("p");
//         tagA.textContent = this.question;
//         root.append(tagA);
//         let flag = true;
//         tagA.addEventListener("click", ()=>{
//             if (flag){
//             tagP.textContent = this.answer;
//             tagA.after(tagP);
//             flag=false;
//         } else{
//             tagP.remove()
//             flag = true;
//         }
//         });
//     };
// }
// const question = new CreateQuestion(questionText, questionAnswer)
// question.renderQuestion();

// ##TASK 2
// Создайте функцию-конструтор объекта stopwatch согласно описанию:
// Свойства объекта: - _time: время; - container: ссылка на DOM-элемент,
// внутри которого нужно выводить время.
// Методы объекта: - start, stop, reset, работающие с его свойствами. -
// setTime и getTime. setTimeбудет получать новое значение времени в
// качестве аргумента, и проверять, является ли оно положительным целым числом.
// Если да - то свойству _time будет присвоено значение аргумента, переданного
// в метод setTime и метод вернет ответ объект вида: { status: "success", }
// Если же аргумент не удовлетворяет критериям, то возвращать объект вида:
// {
// status: "error",
// message: "argument must be positive integer"
// }
// Метод же getTime будет просто возвращать значение свойства _time для
// использования его вне методов объектов.
// <p id="time"></p>
// <button id="start-time">Start</button>
// <button id="stop-time">Stop</button>
// <button id="reset-time">Reset</button>
// function Stopwatch(container) {
// }
// const startBtn = document.getElementById('start-time'); const stopBtn =
// document.getElementById('stop-time'); const resetBtn =
// document.getElementById('reset-time');
// const stopWatchContainer = document.getElementById('time'); const stopwatch =
// new Stopwatch(stopWatchContainer);
// startBtn.addEventListener('click', stopwatch.start.bind(stopwatch));
// stopBtn.addEventListener('click', stopwatch.stop.bind(stopwatch));
// resetBtn.addEventListener('click', stopwatch.reset.bind(stopwatch))

function Stopwatch(container) {
    this._time = 0;
    this.container = container;
    this.start = function(){
        this.container.textContent = this._time;
        this.interval = setInterval(function (){
            this.setTime(this._time+1)
            this.container.textContent = this._time;
        }.bind(this), 1000);
    };
    this.stop = function(){
        clearInterval(this.interval);
    };
    this.reset = function(){
        this._time =0;
        this.container.textContent = this._time;
    };
    this.setTime = function(time){
        if(Number.isInteger(time)){
            this._time = time;
            return {status:"sucess"}            
        } else{
            return {status:"error",
        message: "argument must be positive"} 
        };
    };
    this.getTime = function(){
        return this._time;
    };
    }
    const startBtn = document.getElementById('start-time'); 
    const stopBtn = document.getElementById('stop-time'); 
    const resetBtn = document.getElementById('reset-time');

    const stopWatchContainer = document.getElementById('time'); 
    const stopwatch = new Stopwatch(stopWatchContainer);

    startBtn.addEventListener('click', stopwatch.start.bind(stopwatch));
    stopBtn.addEventListener('click', stopwatch.stop.bind(stopwatch));
    resetBtn.addEventListener('click', stopwatch.reset.bind(stopwatch))