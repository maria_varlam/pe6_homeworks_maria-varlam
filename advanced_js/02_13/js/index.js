"use strict";

// function User(name, age) {
//     this.name = name;
//     this.age = age;
// }
// const user = new User("uasya", 18);
// console.log(user.__proto__);
// console.log(User.prototype);
// console.log(User.prototype === user.__proto__); //вернет true

// User.prototype.getName = function (){
//     return this.name;
// }
// console.log(user.getName())

// function Us() {}
// Us.prototype = {
//     name: "Nadi",
//     getName:function (){
//         return this.name;
//     }
// }
// const us = new Us();
// console.log(us);
// console.log(us.name)

// class User { //type: function
// constructor (name, age){
//     this.name = name;
//     this.age = age;
//     this.login = this._getLogin(); //создается в момент работы constructor
// } 
// set age (value){
//     this._age = value;
// }
// get age (){
//     return this._age;
// }
// static getAge(obj){
//     return obj.age;
// }
// _getLogin(){
//     return this.name.toLowerCase()+this.age;
// }
// someMethod(){
//     console.log(`User method`)
// }
// }
// const user = new User("Mari", 21) //new starting constructor
// console.log(user.age)
// // console.log(user.getAge())
// console.log(User.getAge(user))
// console.log(Object.hasOwn(user, 'name'))

// class Admin extends User {
//     constructor(name, age, salary){
//         super(name, age); // попадают в Admin из User
//         this.salary = salary;
//     }
//     someMethod(){
//         super.someMethod()
//         console.log(`Admin method`)
//     }
// }
// const admin = new Admin ("Arkadiy", 19, 12222)
// console.log(admin)
// admin.someMethod()

/*
TASK 1
Напишите класс Пациент с такими параметрами:
ФИО;
дата рождения;
пол;
И на его основе создайте 2 расширенных класса:
ПациентКардиолога, с дополнительными параметрами:
среднее давление;
перенесенные проблемы с сердечно-сосудистой системой;
ПациентСтоматолога с дополнительными параметрами:
дата последнего визита;
текущее лечение; */

// class Patient{
// constructor(props){
//     this.name = props.name;
//     this.dateofBirth = props.birth;
//     this.gender = props.gender;
// }
// }
// const patient = new Patient({name:"Jake", dateofBirth: 1988, gender:"male"}) 
// console.log(patient)

// class Cardiologist extends Patient{
//     constructor(props){
//         super(props);
//         this.avgBloodPressure = props.avgBloodPressure;
//         this.heartProblems = props.heartProblems;
//     }
// }
// const heartPatient = new Cardiologist({name:"Luna", dateofBirth: 1998, gender:"female", avgBloodPressure:"15", heartProblems:"hhhh"}) 
// console.log(heartPatient)

// class Stomatologist extends Patient{
//     constructor(props){
//         super(props);
//         this.dateodLastVisit = props.dateodLastVisit;
//         this.currentTreatment = props.currentTreatment;
//     }
// }
// const stomPatient = new Stomatologist({name:"Kate", dateofBirth: 1995, gender:"male", dateodLastVisit:"5550", currentTreatment:"jjjjjjj"}) 
// console.log(stomPatient)
/*
/*
TASK 2
Задание: напишите класс Modal, который будет создавать объект, описывающий
всплывающее окно. Параметры объекта: - id всплывающего окна; - классы
всплывающего окна; - текст внутри тега p;
У объекта должно быть 3 метода: render, который возвращает DOM-элемент
всплывающего окна с такой разметкой:
  <div id="idОкна" class="классыОкна">
  <div class="modal-content">
  <span class="close">&times;</span>
  <p>ТекстОкна</p>
  </div>
  </div>
openModal, который открывает окно (его нужно использовать как обработчик click
для button с id="myBtn"); closeModal - который закрывает окно при клике на
крестик (span с классом close) внутри окна
//   <style>
//   body {
//   font-family: Arial, Helvetica, sans-serif;
//   }

//   /* The Modal (background) */
//   .modal {
//     display: none;
//     /* Hidden by default */
//     position: fixed;
//     /* Stay in place */
//     z-index: 1;
//     /* Sit on top */
//     padding-top: 100px;
//     /* Location of the box */
//     left: 0;
//     top: 0;
//     width: 100%;
//     /* Full width */
//     height: 100%;
//     /* Full height */
//     overflow: auto;
//     /* Enable scroll if needed */
//     background-color: rgb(0, 0, 0);
//     /* Fallback color */
//     background-color: rgba(0, 0, 0, 0.4);
//     /* Black w/ opacity */
//     }
  
//     .modal.active {
//     display: block;
//     }
  
//     /* Modal Content */
//     .modal-content {
//     background-color: #fefefe;
//     margin: auto;
//     padding: 20px;
//     border: 1px solid #888;
//     width: 80%;
//     }
  
//     /* The Close Button */
//     .close {
//     color: #aaaaaa;
//     float: right;
//     font-size: 28px;
//     font-weight: bold;
//     }
  
//     .close:hover,
//     .close:focus {
//     color: #000;
//     text-decoration: none;
//     cursor: pointer;
//     }
  
//     </style>
  
//     <div id="root"></div>
//     <button id="myBtn">Open Modal</button>
  
//   class Modal {
//       constructor(id,classes,text){
//           this.id = id;
//           this.classes = classes;
//           this.text = text;
//       }
//       render(){
//           const mainDiv = document.createElement('div');
//           const contentDiv = document.createElement('div');
//           const closeSpan = document.createElement('span');
//           const textContent = document.createElement('p');
//           mainDiv.append(contentDiv);
//           contentDiv.append(closeSpan);
//           contentDiv.append(textContent);
//           mainDiv.setAttribute('id', this.id);
//           mainDiv.classList.add(...this.classes);
//           contentDiv.classList.add("modal-content");
//           closeSpan.classList.add("close");
//           closeSpan.innerHTML = "&times;"
          
//           closeSpan.addEventListener("click", this.closeModal);

//           textContent.textContent = this.text;
//           this.mainDiv=mainDiv;
//           return mainDiv;
//       }
//       openModal(){
//           this.mainDiv.classList.add("active");
//       }
//       closeModal = () =>{
//               this.mainDiv.classList.remove("active");
//       }
//   }
//   const myModal = new Modal('firstModal', ['modal', 'myClass'], "nytext");
//   const root = document.getElementById('root');
//   const closeBtn = document.getElementById("myBtn")
//   root.append(myModal.render());
//   closeBtn.addEventListener('click', function(){
//       myModal.openModal();
//   })
//   */
//   /*
//   TASK 3
//   Возьмите код из предыдущей задачи и создайте один универсальных класс, который
//   отвечает за каркас всплывающего окна, и на его основе 2 других:
//   Вплывающее окно с формой регистрации. Его HTML-разметка будет выглядть так:
  
//   <div id="idОкна" class="классыОкна">
//   <div class="modal-content">
//   <span class="close">&times;</span>
//   <form action="" id="register-form">
//   <input type="text" name="login" placeholder="Ваш логин" required>
//   <input type="email" name="email" placeholder="Ваш email" required>
//   <input type="password" name="password" placeholder="Ваш пароль" required>
//   <input type="password" name="repeat-password" placeholder="Повторите пароль" required>
//   <input type="submit" value="Регистрация">
//   </form>
//   </div>
//   </div>
  
//   Всплывающее окно с формой авторизации. Его HTML-разметка:
  
//   <div id="idОкна" class="классыОкна">
//   <div class="modal-content">
//   <span class="close">&times;</span>
//   <form action="" id="register-form">
//   <input type="text" name="login" placeholder="Ваш логин или email" required>
//   <input type="password" name="password" placeholder="Ваш пароль" required>
//   <input type="submit" value="Вход">
//   </form>
//   </div>
//   </div>
  
//   Привяжите открытие первого окна к кнопке Регистрация, а второго - к кнопке Вход
  
//     <style>
//     body {
//     font-family: Arial, Helvetica, sans-serif;
//     }
  
//     /* The Modal (background) */
//     .modal {
//     display: none;
//     /* Hidden by default */
//     position: fixed;
//     /* Stay in place */
//     z-index: 1;
//     /* Sit on top */
//     padding-top: 100px;
//     /* Location of the box */
//     left: 50%;
//     top: 50%;
//     transform: translate(-50%, -50%);
//     width: 200px;
//     /* Full width */
//     /*height: 100px;*/
//     /* Full height */
//     overflow: auto;
//     /* Enable scroll if needed */
//     background-color: rgb(0, 0, 0);
//     /* Fallback color */
//     background-color: rgba(0, 0, 0, 0.4);
//     /* Black w/ opacity */
//     background-color: #fefefe;
//     padding: 20px;
//     border: 1px solid #888;
//     }
//     .modal.active {
//     display: block;
//     }
//     /* The Close Button */
//     .close {
//     color: #aaaaaa;
//     float: right;
//     font-size: 28px;
//     font-weight: bold;
//     }
//     .close:hover,
//     .close:focus {
//     color: #000;
//     text-decoration: none;
//     cursor: pointer;
//     }
//     </style>
  
// class Modal {
//     constructor(id,classes){
//         this.id = id;
//         this.classes = classes;
//     }
//     render(){
//         const mainDiv = document.createElement('div');
//         const contentDiv = document.createElement('div');
//         const closeSpan = document.createElement('span');
//         mainDiv.append(contentDiv);
//         contentDiv.append(closeSpan);
        
//         mainDiv.setAttribute('id', this.id);
//         mainDiv.classList.add(...this.classes);
//         contentDiv.classList.add("modal-content");
//         closeSpan.classList.add("close");
//         closeSpan.innerHTML = "&times;"
        
//         closeSpan.addEventListener("click", this.closeModal);

//         this.mainDiv=mainDiv;
//         return mainDiv;
//     }
//     openModal(){
//         this.mainDiv.classList.add("active");
//     }
//     closeModal = () =>{
//             this.mainDiv.classList.remove("active");
//     }
// }


// class Register extends Modal{
//     constructor (id, classes){
//         super(id, classes);
//     }
//     render(){
//         super.render();
//         const form = document.createElement("form");
//         const loginInput = document.createElement('input');
//         const emailInput = document.createElement("input");
//         const passwordInput = document.createElement("input");
//         const repeatPassword = document.createElement("input");
//         const submit =document.createElement("input");

//         form.id = "register-form";
//         loginInput.type = "text";
//         loginInput.placeholder="Ваш логин"
//         loginInput.required=true;

//         emailInput.type = "email";
//         emailInput.name = "email";
//         emailInput.placeholder="email"
//         emailInput.required=true;

//         passwordInput.type = "password";
//         passwordInput.name = "password";
//         passwordInput.placeholder="password"
//         passwordInput.required=true;

//         repeatPassword.type = "password";
//         repeatPassword.name = "repeat-password";
//         repeatPassword.placeholder="Please repeat password"
//         repeatPassword.required=true;

//         submit.type = "submit";
//         submit.value = "Регистрация"

//         form.append(loginInput, emailInput, passwordInput, repeatPassword, submit);

//         this.mainDiv.children[0].append(form)
//         return this.mainDiv;
//     }
    
// }

// class Auth extends Modal{
//     constructor (id, classes){
//         super(id, classes);
//     }
//     render(){
//         super.render();
//         const form = document.createElement("form");
//         const loginInput = document.createElement('input');
//         const passwordInput = document.createElement("input");
//         const submit =document.createElement("input");

//         form.id = "register-form";
//         loginInput.type = "text";
//         loginInput.placeholder="Ваш логин"
//         loginInput.required=true;

//         passwordInput.type = "password";
//         passwordInput.name = "password";
//         passwordInput.placeholder="password"
//         passwordInput.required=true;

//         submit.type = "submit";
//         submit.value = "Регистрация"

//         form.append(loginInput, passwordInput,  submit);

//         this.mainDiv.children[0].append(form)
//         return this.mainDiv;
//     }
    
// }
// const root = document.getElementById('root');
// const closeBtn = document.getElementById("myBtn")
// const authBtn = document.getElementById("auth")
// const registerBtn = document.getElementById("register")

// const register = new Register('firstModal', ['modal', 'myClass']);
// root.append(register.render());
// const auth = new Auth('firstModal', ['modal', 'myClass']);
// root.append(auth.render());
// registerBtn.addEventListener('click', function(){
//     register.openModal();
// })
// authBtn.addEventListener('click', function(){
//     auth.openModal();
// })
//   */
//   TASK 4
//   Напишите универсальный класс Input, который будет создавать однострочное поле
//   ввода. У него будут такие параметры: - тип (text, email, password, number, date,
//   submit и т.д.); - name; - обязательное поле или нет; - id; - классы; -
//   placeholder; - errorText - выводит текст ошибки, если поле обязательно к
//   заполнению и не было заполненно;
//   А также такие методы как:
  
//   render - возвращает HTML-разметку формы;
//   handleBlur - срабатывает, если поле обязательно к заполнению и не было
//   заполнено.
  
//   Также напишите класс Form, который будет создавать HTML-форму, и у которого
//   будут такие параметры:
  
//   id;
//   классы;
//   action;
  
//   А также четыре метода:
//   render - который создает каркас формы и возвращает его;
//   handleSumbit - который отвечает за обработку отправки формы;
//   serialize, который все заполненые поля формы собирает в строку формата:
//   "nameПоля=значениеПоля";
//   serializeJSON, который все заполненные поля формы собирает в объект, ключами
//   которого будут значения name

class Input{
    constructor(props){
        this.type = props.type;
        this.name = props.name;
        this.isRequaired = props.isRequaired;
        this.id = props.id;
        this.clases = props.clases;
        this.placeholder = props.placeholder;
        this.errorText = props.errorText;
        this.value=props.value;
    }
    render(){
        const input = document.createElement('input');
        if(this.type){
        input.type = this.type;
        }
        if(this.name){
        input.name = this.name;
        }
        if(this.isRequaired){
        input.required = this.isRequaired;
        }
        if(this.clases && this.clases.lenght > 0){
        input.classList.add(...this.clases);
        }
        if(this.placeholder){
        input.placeholder = this.placeholder;
        }
        if(this.value){
        input.value=this.value;
        }
        if(this.id){
            input.id = this.id;
        }
        input.addEventListener('blur', handleBlur.bind(this))
        return input;
    }
    handleBlur(e){
        if (e.target.required && e.target.value.trim() === "") {
            alert(this.errorText);
        }

    }
}

class Form {
    constructor(id, clases, action){
        this.id = id;
        this.clases=clases;
        this.action = action; 
    }
    render(){
        const form = document.createElement('form');
        form.setAttribute("id", this.id);
        form.classList.add(...this.clases);
        form.setAttribute("action", this.action);
        form.addEventListener("submit", this.handleSumbit)
        return form;
    }
    handleSumbit(e){
        e.preventDefault()
    }
    serialize(){}
    serializeJSON(){}
}

class Modal {
    constructor(id,classes){
        this.id = id;
        this.classes = classes;
    }
    render(){
        const mainDiv = document.createElement('div');
        const contentDiv = document.createElement('div');
        const closeSpan = document.createElement('span');
        mainDiv.append(contentDiv);
        contentDiv.append(closeSpan);
        
        mainDiv.setAttribute('id', this.id);
        mainDiv.classList.add(...this.classes);
        contentDiv.classList.add("modal-content");
        closeSpan.classList.add("close");
        closeSpan.innerHTML = "&times;"
        
        closeSpan.addEventListener("click", this.closeModal);

        this.mainDiv=mainDiv;
        return mainDiv;
    }
    openModal(){
        this.mainDiv.classList.add("active");
    }
    closeModal = () =>{
            this.mainDiv.classList.remove("active");
    }
}


class Register extends Modal{
    constructor (id, classes){
        super(id, classes);
    }
    render(){
        super.render();
        const form = document.createElement("form");
        const loginInput = new Input({type: "text", name: "login", isRequaired:true, id:"login",  placeholder:"Ваш логин"}).render();
        const emailInput = new Input({type: "email", name: "email", isRequaired:true,   placeholder:"Ваш email"}).render();
        const passwordInput = new Input({type: "password", name: "password", isRequaired:true, id:"password", placeholder:"Ваш password"}).render();
        const repeatPassword = new Input({type: "password", name: "repeat-password", isRequaired:true, placeholder:"Please repeat password"}).render();
        const submit =new Input({type: "submit",  id:"submit", value:"Регистрация"}).render();

        form.id = "register-form";

        form.append(loginInput, emailInput, passwordInput, repeatPassword, submit);

        this.mainDiv.children[0].append(form)
        return this.mainDiv;
    }
    
}

class Auth extends Modal{
    constructor (id, classes){
        super(id, classes);
    }
    render(){
        super.render();
        const form = document.createElement("form");
        const loginInput = new Input({type: "text", name: "login", isRequaired:true, id:"login",  placeholder:"Ваш логин"}).render();
        const passwordInput = new Input({type: "password", name: "password", isRequaired:true, id:"password", placeholder:"Ваш password"}).render();
        const submit = new Input({type: "submit",  id:"submit", value:"Регистрация"}).render();

        form.id = "register-form";
    
        form.append(loginInput, passwordInput,  submit);

        this.mainDiv.children[0].append(form)
        return this.mainDiv;
    }
    
}
const root = document.getElementById('root');
const closeBtn = document.getElementById("myBtn")
const authBtn = document.getElementById("auth")
const registerBtn = document.getElementById("register")

const register = new Register('firstModal', ['modal', 'myClass']);
root.append(register.render());
const auth = new Auth('firstModal', ['modal', 'myClass']);
root.append(auth.render());
registerBtn.addEventListener('click', function(){
    register.openModal();
})
authBtn.addEventListener('click', function(){
    auth.openModal();
})




  /*
  TASK 5
  Возьмите код из предыдущей задачи, и создайте два дочерних класса RegisterForm и
  LoginForm.
  
  RegisterForm:
  метод render будет возращать форму с 4 полями ввода: Логин, email, Пароль и
  ПовторитеПароль. Для отрисовки полей используйте класс Input из предыдущего
  задания.

  метод handleSumbit будет дополнительно проверять, совпадают ли пароли из
  полей Пароль и ПовторитеПароль.
  
  LoginForm:
  метод render будет возращать форму с 2 полями ввода: Логин/email и Пароль.
  Для отрисовки полей используйте класс Input из предыдущего задания.
  метод handleSumbit будет проверять заполнены ли оба поля ввода.
  
  */