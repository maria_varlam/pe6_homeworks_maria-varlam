"use strict";

// let a = 3;
// function dn(){
//     return a+3;
// }

// console.log(dn())

// function fn(){
//     let a =3;
//     return function(b) {
//         return a+3;
// }
// }
// const res = fn();
// console.log(res(2))
// console.log(res(3))
// console.log(res(4))

// function fn(){
//     let a =3;
//     return function(b) {
//         return a+b;
// }
// }
// const res = fn();
// console.log(res(2))
// console.log(res(3))
// console.log(res(4))

// try{
// function fn(){
//     return b;
// }
// console.log(fn())
// console.log("here")
// } catch(e){
//     console.dir(e)
//     console.dir(e.message) //выводит почему ошибка(текст ошибки)
// }

// console.log("what")


    // function fn(b){
    //     try{
    //     return b.remove();
    
    // } catch(e){
    //     // console.dir(e)
    //     // console.dir(e.message)

    //     throw new Error(e.message) //сами сосдаем ошибки (проброс ошибки) таким способом
    //     // throw {name: "Error"} //или таким
  
    // }}

    // try{
    //     console.log(fn("r"))
    // } catch (e){
    //     console.log(e.message)
    // }
    
    // console.log("what")

    //IIEF - функциональное выражение анонимной самовызываемой функ
    // (function(){let b = 3})();

    // let a = 3;
    // (function(){let b = 3; console.log(a)})(a);

    // let b = 3;
    // (function(){let b = 13; console.log(b)})(b);'

    // let d = 3;
    // let b = 4;
    // (function(c){
    //     let a = 13; 
    //     console.log(c);
    //     console.log(a);
    //     console.log(b);
    //     console.log(d);
    // })(b);


/* TASK 1
   Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi autem dolores expedita natus omnis. Accusamus assumenda error incidunt maxime nisi. Culpa fugit itaque labore laudantium molestias nam nesciunt repellat totam.
   Увеличить
   Уменьшить
Напишите функцию changeTextSize, у которой будут такие аргументы:

Ссылка на DOM-элемент, размер текста которого нужно изменить без регистрации
и sms.
Величина в px, на которую нужно изменить текст, возвращает функцию, меняющую
размер текста на заданную величину.

С помощью этой функции создайте две:

одна увеличивает текст на 2px от изначального;
вторая - уменьшает на 3px.

После чего повесьте полученнные функции в качестве обработчиков на кнопки с
id="increase-text" и id="decrease-text". */

// const text = document.querySelector('#text');
// const increaseBtn = document.querySelector('#increase-text');
// const decreaseBtn = document.querySelector('#decrease-text');
// class changeTextSize{
//     constructor(src){
//         this.src = src;
//         this.sizeBefore = parseFloat(this.src.style.fontSize);
//     }
//     increase(size){
//         this.sizeBefore += size;
//         this.src.style.fontSize = this.sizeBefore + "px";
//     }
//     decrease(size){
//         this.sizeBefore -= size;
//         this.src.style.fontSize = this.sizeBefore + 'px';
//     }
// }
// const newSize = new changeTextSize(text);
// increaseBtn.addEventListener('click', ()=>{newSize.increase(2)});
// decreaseBtn.addEventListener('click', ()=>{newSize.decrease(3)});

/* TASK 2
Напишите class createProduct, который будет создавать объекты, описывающие
товары. У товара должны быть такие свойства:

name;
fullName;
article;
price. При этом при попытке напрямую (через точку) изменить свойство price
происходит его его проверка на прравильность: цена должна быть целым
положительным числом. Если эти требования нарушаются - присвоения не
произойдет. Создавать его аналог через _price нельзя.

Пример работы: const notebook = createProduct("lenovo X120S", "lenovo X120S
(432-44) W", 3332, 23244); console.log(notebook.price);// выведет 23244
notebook.price = -4; // присвоение не произойдет console.log(notebook.price);//
выведет 23244 notebook.price = 22000; console.log(notebook.price);// выведет
22000
*/

class Product{
    constructor(name, fullName, article, price){
        this.name=name;
        this.fullName=fullName;
        this.article=article;
        this.price=price;
        Object.defineProperty(this, 'price', {
            get: function(){},
            set: function(value){this.price =value}
        })
    }
}
const notebook = new Product(
    "lenovo X120S", 
    "lenovo X120S(432-44) W",
    3332, 
    23244); 

console.log(notebook.price);// выведет 23244
notebook.price = -4; // присвоение не произойдет 
console.log(notebook.price);//выведет 23244 notebook.price = 22000; 
console.log(notebook.price);// выведет 22000



/*
TASK 3
Создайте объект Slider, который будет получать такие аргументы:

эффект прокрутки
скорость прокрутки
автостарт (да/нет)
список слайдов (в виде массива объектов)

И методами:

добавить слайд
удалить слайд
получить слайд

Все возможные на ваш взгляд проблемы оберните в конструкции try ... catch
*/
/* TASK 4 



Создайте объект Validation, который будет иметь такие методы:

проверка правильности ввода email
проверка правильности ввода мобильного телефона
проверка правильности ввода паспорта (первые два символа - буквы, потом -
цифры)

Сделайте так, чтобы проверка происходила в момент нажатия клавиши, и ненужные
символы не появлялись в поле ввода вообще.

*/