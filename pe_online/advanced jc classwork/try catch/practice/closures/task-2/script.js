/**
 *   Напишите функцию createProduct, которая будет создавать объекты, описывающие товары. У товара должны быть такие свойства:
 *    - name;
 *    - fullName;
 *    - article;
 *    - price.
 *    При этом при попытке напрямую (через точку) изменить свойство price происходит его проверка на правильность:
 *    цена должна быть целым положительным числом. Если эти требования нарушаются - присвоения не произойдет.
 *    Создавать его аналог через _price нельзя.
 *
 *    Пример работы:
 *    const notebook = createProduct("lenovo X120S", "lenovo X120S (432-44) W", 3332, 23244);
 *    console.log(notebook.price);// выведет  23244
 *    notebook.price = -4; // присвоение не произойдет
 *    console.log(notebook.price);// выведет  23244
 *    notebook.price = 22000;
 *    console.log(notebook.price);// выведет  22000
 *   
 */

function createProduct(name, fullName, article, price) {
    if (price > 0 && Number.isInteger(price)) {
        return {
            name, 
            fullName, 
            article, 
        set price (value){
            if (value > 0 && Number.isInteger(value)) {
            price = value;
            }
        },
        get price(){
            return price;
        }
    };
    } else return {};

    
}

const notebook = createProduct("lenovo", "lenovo X",3332,4);
console.log(notebook);
console.log(notebook.price);