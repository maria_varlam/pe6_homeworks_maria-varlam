/**
 * Напишите функцию changeTextSize, у которой будут такие аргументы:
 * 1. Ссылка на DOM-элемент, размер текста которого нужно изменить без регистрации и sms.
 * 2. Величина в px, на которую нужно изменить текст,  возвращает функцию, меняющую размер текста на заданную величину.
 *
 * С помощью этой функции создайте две:
 * - одна увеличивает текст на 2px от изначального;
 * - вторая - уменьшает на 3px.
 *
 * После чего повесьте полученные функции в качестве обработчиков на кнопки с id="increase-text" и id="decrease-text".
 *
 */

let elementIncrease = document.querySelector('#increase-text');
let elementDecrease = document.querySelector('#decrease-text');
let elementText = document.querySelector('#text');
let elementsSize = parseInt(elementText.style.fontSize);

function changeTextSize(element,size) {
    return function (){
        if ((elementsSize+size)>=12 && (elementsSize+size) <=100) {
            elementsSize += size;
            element.style.fontSize = `${elementsSize}px`  
        }
        console.log(elementsSize);
    }   
}
const upSize = changeTextSize(elementText,2);
const downSize = changeTextSize(elementText,-3);
elementIncrease.addEventListener('click', upSize);
elementDecrease.addEventListener('click', downSize);