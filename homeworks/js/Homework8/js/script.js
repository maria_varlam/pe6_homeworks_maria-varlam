// Опишите своими словами, как Вы понимаете, что такое обработчик событий.

"use strict";

const root = document.querySelector("#root");
const input = document.createElement("input");
root.textContent = `Price: `;
input.addEventListener("focus", function(){input.style.cssText ='outline:1px solid green';});
root.append(input);
input.addEventListener("blur", function(){
    if (input.value>0){
    const span = document.createElement("span");
    span.textContent = `Текущая цена: ${input.value}`;
    root.before(span);
    const button = document.createElement("button");
  button.textContent = "X";
  span.after(button);
  input.classList.add("green_value")
  button.addEventListener("click", function(){
      span.remove();
      button.remove();
      input.value = "";
  })}
  if (input.value<0){
          input.style.cssText = `outline:1px solid red`;
          const p = document.createElement("p");
          p.textContent = "Please enter correct price";
          input.after(p)
      }
})
