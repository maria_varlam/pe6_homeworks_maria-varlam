// Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM). Это модель докуиента, которая представляет все содержимое страницы в виде объектов, которые можно изсенить.

"use strict";

const arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

function showList(arr, parent){
    parent = document.body;
    const ul = document.createElement("ul");
    const newArr = arr.map((item)=>{
        const li = `<li>${item}</li>`;
        return li;
    });
    ul.innerHTML =newArr.join('');
    return ul;
}
document.body.before(showList(arr, parent));
