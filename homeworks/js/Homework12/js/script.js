"use strict";

const imagesWrapper = document.querySelector(".images-wrapper");
const btnStop = document.createElement("button");
const btnContinue = document.createElement("button");
btnStop.textContent = "Прекратить";
btnContinue.textContent = "Возобновить показ";
document.body.before( btnStop, btnContinue);
const images = [...imagesWrapper.querySelectorAll(".image-to-show")];
let activeIndex = 0;
let timer;

function showImg(){
     timer = setInterval(function() {
        images.forEach((element, index) => {
          if (element.classList.contains("active")) {
            if (index === images.length - 1) {
              activeIndex = 0;
            } else {
              activeIndex = index + 1;
            }
          }
          element.classList.remove("active");
        });
        images[activeIndex].classList.add("active");
      }, 3000);
    }
    showImg()
btnStop.addEventListener("click", ()=>{
    clearInterval(timer)
})
btnContinue.addEventListener("click", ()=>{
    clearInterval(timer)
    showImg()
})

