"use strict";

const content = document.getElementsByClassName("tab_text");
const clickHandler = (event)=>{
const clickedLi = event.target;
const active = document.getElementsByClassName("active");
if (active.length>0) active[0].classList.remove("active")
clickedLi.classList.add("active");
for (let el of content) {
   el.classList.add("hidden");
   if (el.dataset.tab === clickedLi.dataset.tab){
       el.classList.remove("hidden")
   }
}
}
const tabs = document.getElementById("tabs");
tabs.addEventListener("click", clickHandler);
