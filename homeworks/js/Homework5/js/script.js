// Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования. Экранирование это использование специальных символов для правильного отображения этих же символов внутри строк.

"use strict";
function createNewUser() {
  let userName = prompt("Please enter your first name");
  let surName = prompt("Please enter your last name");
  let birthday = prompt("Please enter your date of birthday (dd.mm.yyyy)");
    return {
    firstName: userName,
    lastName: surName,
    birthdayDate: birthday,
    getLogin: function () {
    return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    },
    getAge: function(){
      const today = new Date();
		const userBirthday = Date.parse(this.birthdayDate.slice(6)-this.birthdayDate.slice(3, 5)-this.birthdayDate.slice(0, 2));
		const age = ((today - userBirthday) / (1000 * 60 * 60 * 24 * 30 * 12)).toFixed(0);
		if (age < today) {
			return `Вам ${age - 1} лет`;
		} else {
			return `Вам ${age} лет`;
		}
    },
    getPassword: function(){
      return this.firstName[0].toUpperCase() + this.lastName.toLowerCase()+this.birthdayDate.slice(-4);
    }
    };
  }
  console.log(createNewUser());
