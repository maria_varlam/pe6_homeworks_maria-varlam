"use strict";
const btn= document.createElement("button");
btn.textContent = "Switch mode";
document.body.before(btn);

btn.addEventListener("click",(e)=>{
    e.preventDefault();
    if(localStorage.getItem("theme")==="dark"){
        localStorage.removeItem("theme");
    }else{
        localStorage.setItem("theme", "dark")
    }
    addDark();
});
function addDark () {
    try {
        if(localStorage.getItem("theme")==="dark"){
            document.querySelector(".page").classList.add("body_page_dark")
        } else{
            document.querySelector(".page").classList.remove("body_page_dark") 
        }
    } catch (error) {
        
    }
}
addDark ();