//Почему для работы с input не рекомендуется использовать события клавиатуры? Потому что для того чтобы ввести значения в input, не всегда используется клавиатура, например может использоваться рукописный ввод или вставляться текст. 

"use strict";

 function onKeyDown (event){
    const clickedKey = event.key.toLowerCase() ;
    const btn = [...document.querySelectorAll(".btn")];
    for (let el of btn) {
        if (clickedKey ===el.dataset.code){
            el.classList.add("btn_clicked")
        } else{
            el.classList.remove("btn_clicked")
        }
    }
}
document.addEventListener("keydown", onKeyDown);