"use strict";

$(function(){
 $("#main_menu").stop().animate({'margin-left':'-700px', 'opacity':'0.8'},1000);
 $("#main_menu").hover(function(){
 $(this).stop().animate({'margin-left':'-100px', 'opacity':'0.9'},
 500,
 'linear',
 function(){
 $("#menu_label").css({'display':'none'});
 });

 }, 
 function(){
 $(this).stop()
 .animate({'margin-left':'-100px', 'opacity':'0.4'},
 500,
 'linear',
 function(){
 $("#menu_label").css({'display':'block'});
 });
 })
 })

$(document).on("click", "#btn_hide", function(){
    $("#posts_panel").slideToggle("500");
    return false;
})
$('.scrollUp').click(function(){
    $("html, body").animate({ scrollTop: 0 }, 700);
    return false;
});