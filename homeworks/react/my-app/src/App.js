import React, { Component } from 'react';
import Button from './Components/Button'
import Modal from './Components/Modal'


class App extends Component {
  state = {
    firstModal: false,
    secondModal: false,
  }

  showFirstModal = () => {
    this.setState({ firstModal: true });
  }
  showSecondModal = () => {
    this.setState({ secondModal: true });
  }

  closeModal = (event) => {
    this.setState({ firstModal: false })
    this.setState({ secondModal: false })
  }

  render() {
    return (
      <div className="App">
        <Button className="buttons" text={"Open first modal"} backgroundColor={"teal"}
          onClick={() => this.showFirstModal()} />
        <Button className="buttons" text={"Open second modal"} backgroundColor={"green"}
          onClick={() => this.showSecondModal()} />
        {this.state.firstModal ?
          <Modal header={"First Modal"} text={"Lorem"} closeButton={true} actions={
            <>
              <button className={'modal__buttons-btn'} onClick={() => this.closeModal()}>Ok</button>
              <button className={'modal__buttons-btn'} onClick={() => this.closeModal()}>Cancel</button>
            </>
          } onClick={(event) => this.closeModal(event)} /> : ''}
        {this.state.secondModal ?
          <Modal header={"Second Modal"} text={"Insert"} closeButton={true} actions={
            <>
              <button className={'modal__buttons-btn'} onClick={() => this.closeModal()}>Apply</button>
              <button className={'modal__buttons-btn'} onClick={() => this.closeModal()}>Cancel</button>
            </>
          } onClick={(event) => event.currentTarget === event.target && this.closeModal()} /> : ''}
      </div>
    )
  }
}

export default App;

