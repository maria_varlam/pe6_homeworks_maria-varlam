//Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript.
// Прототипне наслідування це можливість об'єкта А наслідувати (повторювати) інформацію від іншого об'єкта Б, зокрема ту інформацію яку А упадковує від Б
//Для чого потрібно викликати super() у конструкторі класу-нащадка?
// super() у класі-нащадка визиває конструктор класа-родителя


"use strict";

class Employee{
    constructor(props){
        this._name=props.name;
        this._age=props.age;
        this._salary=props.salary;
    }
    get name (){
        return this._name
    }
    set name (value){
        this._name = value
    }
    get age (){
        return this._age
    }
    set age (value){
        this._age = value
    }
    get salary (){
        return this._salary
    }
    set salary (value){
        this._salary = value
    }
}
class Programmer extends Employee{
    constructor(props){
        super(props);
        this.lang = props.lang; 
    }
    get salary (){
        return this._salary*3
    }
}
const mary = new Programmer({name:"Mary", age:"25", salary:"12000", lang:"ua"});
console.log(mary)
const jacke = new Programmer({name:"Jacke", age:"33", salary:"22000", lang:"ua, en, ru"});
console.log(jacke)
const lari = new Programmer({name:"Lari", age:"52", salary:"30000", lang:"ua, en, de, ru, ge, fr"});
console.log(lari)
