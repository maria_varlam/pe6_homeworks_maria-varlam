
"use strict";

const API = "https://ajax.test-danit.com/api/swapi/films";
const root = document.querySelector('#films');
const charactersArr = [];

const getFilm = (url)=>{
	fetch(url)
	.then(response=>response.json())
	.then(renderFilms)
}


const renderFilms = (data) => {
		console.log(data)
		console.log(data[1].characters)
		let res = '';
		data.forEach(({episodeId, name, openingCrawl, characters}) => {
			charactersArr.push(characters);
			res += `
			<ul class="film-list">
			<li class = "film-item">
						<span class="film-name name"></span>${name}
					</li>
			<li class = "film-item">
						<span class="film-name">Episode</span>: ${episodeId}
					</li>
				
					<li class = "film-item">
						<span class="film-name">Opening Crawl</span>: ${openingCrawl}
					</li>
					<li class = "film-item">
						<div class="actors"></div>
					</li>
			</ul>`;
		})
		getCharacters(charactersArr)
		return root.innerHTML=res;
	}

	const randerCharacters = (charactersArr, actors, index) => {
		const actorsList = document.createElement("ul");
		actorsList.classList.add("actors__list");
	  
		charactersArr.forEach(({ name }) => {
		  actorsList.innerHTML += `
							  <li class="actors__name">${name}</li>
						  `;
		});

		actors[index].innerHTML = `<h2 class="actors__title">Actors:</h2>`;
		actors[index].append(actorsList);
	  };
	  
	  const getCharacters = (charactersArr) => {
		charactersArr.forEach((characters, index) => {
		  const promiceArr = [];
	  
		  characters.forEach((characterUrl) => {
			promiceArr.push(fetch(characterUrl).then((response) => response.json()));
		  });
	  
		  Promise.all(promiceArr)
			.then((charactersArr) => {
			  const actors = document.querySelectorAll(".actors");
			  randerCharacters(charactersArr, actors, index);
			})
			.catch((err) => alert(err.message));
		});
	  };	


getFilm(API)

