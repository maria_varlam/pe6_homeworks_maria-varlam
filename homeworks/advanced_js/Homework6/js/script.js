"use strict";

const API = "http://api.ipify.org/?format=json";
const info = "http://ip-api.com/";

const buttonIP = document.querySelector('#btn')
const userContainer = document.querySelector('#userInfo');
buttonIP.after(userContainer)

const sendRequest = async (url, method = "GET")=>{
return await fetch (url,{method})
.then (response => {
	if(response.ok){
		return response.json()
	} else{
		return new Error ("Error")
	}
})
}

buttonIP.addEventListener('click', async()=>{
	const getInfo = await sendRequest(API);
	const result = await sendRequest(`${info}json/${Object.values(getInfo)}`)
	let {timezone, country, regionName, city, zip} = result;
	userContainer.innerHTML = `<strong>You are from:</strong>  ${timezone},  ${country},  ${regionName},  ${city},  ${zip}`;
})
