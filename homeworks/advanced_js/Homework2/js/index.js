//Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch. 
//Наприклад при введенні в input неправильного значення можемо перехоплювати помилки і ви водити їх - потрібно ввести значення від 5 до 7 , а якщо ми вводимо значення не з цього діапазону можна перехоплювати цю помилку та відображувати що саме не так
//Коли кодин написаний неправильно граматично - albert замість alert

"use strict";

const books = [
  { 
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70 
  }, 
  {
   author: "Сюзанна Кларк",
   name: "Джонатан Стрейндж і м-р Норрелл",
  }, 
  { 
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  }, 
  { 
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  }, 
  {
   author: "Террі Пратчетт",
   name: "Рухомі картинки",
   price: 40
  },
  {
   author: "Анґус Гайленд",
   name: "Коти в мистецтві",
  }
];


const root = document.getElementById('root');
const renderBook = (arrBooks)=>{
  arrBooks.forEach(book => {
    const [validationResult, emptyFields] = validateFields(book);
    try {
      if(!validationResult){
        throw new Error(`noFields:${emptyFields.join(",  ")}`)
      } else{
        createElement(book);
      }
    } catch (error) {
      console.log(error)
    }
  });
}
const validateFields = (book)=>{
  const requireFields = ["author", "name", "price"]
  const bookFields = Object.keys(book)
  let validateResult = true;
  let emptyFields = [];
  if (requireFields.length===bookFields.includes(requireFields)){
    validateResult = true;
  }
  requireFields.forEach(field=>{
    const isFieldinBook = bookFields.includes(field)
    if(!isFieldinBook){
      emptyFields.push(field);
      validateResult = false;
    }
  })
  return [validateResult, emptyFields]
}
const createElement = (book)=>{
 for (const key in book) {
   const ul = document.createElement("ul");
   const li = document.createElement("li");
   li.innerHTML=book[key];
   ul.appendChild(li);
   root.appendChild(ul);
 }
}
renderBook(books);