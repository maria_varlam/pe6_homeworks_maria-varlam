import * as flsFunctions from "./modules/functions.js";

flsFunctions.isWebp();

$(document).ready(function () {
    $('.hat-header__burger').click(function (event) {
        $('.hat-header__burger,.hat-header__menu').toggleClass('active');
    });
});
