"use strict";

// var, let, const-создание переменных

// var MAX_LENGTH - название переменной которая 100% будет const (от нее зависит работа кода)
// При создании констант значение присваиваются сразу
// var MAX_LENGTH = 30;

// let userName = "Uasya";

// userName = 3;
// const maxLenght = 30;

// const myString = "Text"; //"", '', `` (`` обратные кавычки над tab)
// console.log(myString);
// console.log(typeof myString);

// const myNumber = 3;
// console.log(myNumber);
// console.log(typeof myNumber);
// console.log(typeof typeof myNumber); //в таком случае тип данных string

// const myNan = NaN;
// console.log(myNan);
// console.log(typeof myNan); //тип данных number

// const myNull = null;
// console.log(myNumber);
// console.log(typeof myNumber); //тип данных object

// const myUnd = undefined;
// console.log(myUnd);
// console.log(typeof myUnd); //тип данных undefined

// const myBool = true; //thue, false
// console.log(myBool);
// console.log(typeof myBool); //тип данных boolean

// const myFunc = function() {};
// console.dir(myFunc); //в Chrome, но можем писать console.log
// console.log(typeof myFunc); //тип данных function

// const myObject = {
//   name: "Uasya",
//   age: 33
// };
// console.log(myObject);
// console.dir(typeof myObject); //тип данных Object

// const myArr = [1, 2, 3, null, undefined];
// console.log(myArr);
// console.dir(typeof myArr); //тип данных Object

// const num = 3;

// alert(num);
// console.log("alert done");

// const answerSimple = confirm("Are you agree?"); //true or false
// console.log(answerSimple);

const answerDif = prompt("How are you?", "all right"); //второе значение идет в поле для ввода
console.log(answerDif); //варианты которые можно получить от пользователя: empty string, null, string,
