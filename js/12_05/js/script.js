"use strict";

//BOM-browser object model
// console.log(window);
// console.log(navigator);
// console.log(location);

//DOM-document object model
// console.dir(document);
// console.dir(document.body);

/*
Задание 1.
Получить и вывести в консоль следующие элементы страницы:
По идентификатору (id): элемент с идентификатором list;
По классу — элементы с классом list-item;
По тэгу — элементы с тэгом li;
По CSS селектору (один элемент) — третий li из всего списка;
По CSS селектору (много элементов) — все доступные элементы li.
Вывести в консоль и объяснить свойства элемента:
innerText;
innerHTML;
outerHTML. */

/**
Задание 2.
Получить элемент с классом .remove.
Удалить его из разметки.
Получить элемент с классом .bigger.
Заменить ему CSS-класс .bigger на CSS-класс .active.
Условия:
Вторую часть задания решить в двух вариантах: в одну строку и в две
строки.*/

// const listRemove = document.querySelector(".list-item.remove");
// console.dir(listRemove);
// listRemove.outerHTML = "";
// listRemove.remove();
// console.dir(listRemove);

// const listBigger = document.querySelector(".list-item.bigger");
// console.dir(listBigger);
// listBigger.classList.remove("bigger");
// listBigger.classList.add("active");
// console.dir(listBigger);
// listBigger.className = listBigger.className.replace("bigger", "active");
// console.dir(listBigger);

/**
Задание 3.
На экране указан список товаров с указанием названия и количества на складе.
Найти товары, которые закончились и:
Изменить 0 на «закончился»;
Изменить цвет текста на красный;
Изменить жирность текста на 600.
Требования:
Цвет элемента изменить посредством модификации атрибута style.
*/

const list = document.getElementsByTagName("li");
for (let i = 0; i < list.length; i++) {
  checkFinished(list[i]);
}
function checkFinished(item) {
  if (item.textContent.split(":")[1].trim() === "0") {
    // item.textContent = item.textContent.replace("0", " закончился");
    item.textContent = item.textContent.split(":")[0] + " закончился";
    item.style.cssText = "color:red; font-weight:600";
  }
}
/**

Задание 4.


Получить элемент с классом .list-item.
Отобрать элемент с контентом: «Item 5».


Заменить текстовое содержимое этого элемента на ссылку, указанную в секции
«дано».


Сделать это так, чтобы новый элемент в разметке не был создан.


Затем отобрать элемент с контентом: «Item 6».
Заменить содержимое этого элемента на такую-же ссылку.


Сделать это так, чтобы в разметке был создан новый элемент.


Условия:


Обязательно использовать метод для перебора;




Объяснить разницу между типом коллекций: Array и NodeList. */

// /_ Дано _/ const targetElement =
// 'Google
// it!';
/*
<ul class="list">
<li class="list-item">Item 1</li>
<li class="list-item">Item 2</li>
<li class="list-item">Item 3</li>
<li class="list-item">Item 4</li>
<li class="list-item">Item 5</li>
<li class="list-item">Item 6</li>
<li class="list-item">Item 7</li>
</ul>
*/
