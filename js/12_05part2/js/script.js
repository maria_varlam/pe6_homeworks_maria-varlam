"use strict";

const root = document.getElementById("root");

// const p = document.createElement("p");
// console.dir(p);
// p.textContent = "lorem";
// // root.innerHTML += p.outerHTML; //добавляет элемент на страницу

// root.append(p);//слева-куда вставить, в скобках то что добавить

/**
Задание 1.
Написать скрипт, который создаст квадрат произвольного размера.
Размер квадрата в пикселях получить интерактивно посредством диалогового окна
prompt.
Если пользователь ввёл размер квадрата в некорректном формате —
запрашивать данные повторно до тех пор, пока данные не будут введены
корректно.
Все стили для квадрата задать через JavaScript посредством одной строки кода.
Тип элемента, описывающего квадрат — div.
Задать ново-созданному элементу CSS-класс .square.
Квадрат в виде стилизированного элемента div необходимо
сделать первым и единственным потомком body документа. */

// let size = ``;
// do {
//   size = prompt("Please enter size");
// } while (+size <= 0 || isNaN(+size));
// const square = document.createElement(`div`);
// square.style.cssText = `width: ${size}px; height: ${size}px; background-color: blue;`;
// document.body.innerHTML = square.outerHTML;

/**

Задание 2.
Написать функцию-фабрику квадратов createSquares.
Функция обладает двумя параметром — количеством квадратов для создания.
Если пользователь ввёл количество квадратов для создания в недопустимом
формате —
запрашивать данные повторно до тех пор, пока данные не будут введены
корректно.
Максимальное количество квадратов для создания — 10.
Если пользователь решил создать более 10-ти квадратов — сообщить ему о
невозможности такой операции
и запрашивать данные о количестве квадратов для создания до тех пор, пока они
не будут введены корректно.
Размер каждого квадрата в пикселях нужно получить интерактивно посредством
диалогового окна prompt.
Если пользователь ввёл размер квадрата в недопустимом формате —
запрашивать данные повторно до тех пор, пока данные не будут введены
корректно.
Цвет каждого квадрата необходимо запросить после введения размера квадрата в
корректном виде.
Итого последовательность ввода данных о квадратах выглядит следующим образом:
Размер квадрата n;
Цвет квадрата n;
Размер квадрата n + 1;
Цвет квадрата n + 1.
Размер квадрата n + 2;
Цвет квадрата n + 3;
и так далее...
Если не любом этапе сбора данных о квадратах пользователь кликнул по кнопке
«Отмена»,
необходимо остановить процесс создания квадратов и вывести в консоль
сообщение:
«Операция прервана пользователем.».
Все стили для каждого квадрата задать через JavaScript за раз.
Тип элемента, описывающего каждый квадрат — div.
Задать ново-созданным элементам CSS-классы: .square-1, .square-2, .square-3 и
так далее.
Все квадраты необходимо сделать потомками body документа. */

const maxSquares = 10;
const createSquares = function(count, maxSquares) {
  if (count > maxSquares) {
    console.log("operation impossible");
    return false;
  }
  for (let i = 0; i < count; i++) {
    let size;
    do {
      size = prompt("Please enter size of square");
      if (size === null) {
        console.log("Операция прервана пользователем.");
        return false;
      }
    } while (+size <= 0 || isNaN(+size));
    let color;
    do {
      color = prompt("Please enter color of square(RGB format)");
      if (size === null) {
        console.log("Операция прервана пользователем.");
        return false;
      }
    } while (!color);
  }
  const square = document.createElement(`div`);
  square.style.cssText = `width: ${size}px; height: ${size}px; background-color: rgb(${color});`;
  document.body.append(square);
};
let count;
do {
  count = prompt("Please enter a number of squares");
} while (+count <= 0 || isNaN(+count));
createSquares(count, maxSquares);

/**

Задание 3.


Написать функцию fillChessBoard, которая развернёт «шахматную» доску 8 на 8.


Цвет тёмных ячеек — #161619.
Цвет светлых ячеек — #FFFFFF.
Остальные стили CSS для доски и ячеек готовы.


Доску необходимо развернуть внутри элемента с классом .board.


Каждая ячейка доски представляет элемент div с классом .cell. */

// /_ Дано _/ const LIGHT_CELL = '#ffffff'; const DARK_CELL = '#161619'; const
// V_CELLS = 8; const H_CELLS = 8;
// const board = document.createElement('section');
// board.classList.add('board');
// document.body.prepend(board);
