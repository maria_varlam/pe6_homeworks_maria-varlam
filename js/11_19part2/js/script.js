"use strict";
/**

Задание 1.
Создать объект пользователя, который обладает тремя свойствами:
Имя;
Фамилия;
Профессия.
А также одним методом sayHi, который выводит в консоль сообщение 'Привет.'.


// const user ={
// name: "Ivan",
// lastName: "Ivanov",
// proffecion: "cleaner",
// sayHi: function(){
//     console.log("hello");
// }
// };
// user.sayHi();
// console.log(user.name);

/**

Задание 2.
Расширить функционал объекта из предыдущего задания:
Метод sayHi должен вывести сообщение: «Привет. Меня зовут ИМЯ ФАМИЛИЯ.»;
Добавить метод, который меняет значение указанного свойства объекта.
Продвинутая сложность:
Метод должен быть «умным» — он генерирует ошибку при совершении попытки
смены значения несуществующего в объекте свойства. */

const user ={
name: "Ivan",
lastName: "Ivanov",
proffecion: "cleaner",
sayHi: function(){
    console.log(`Привет. Меня зовут ${user.name}  ${user.lastName}.`);
},
changeProperty: function(propertyName, propertyValue){
// this.name=propertyValue;//меняет конкретное свойство
if(Object.hasOwn(this, propertyName){
    this[propertyName] = propertyValue;
} else {
    console.log("Error");
}
}
};
user.sayHi();
user.changeProperty("name", "Uasya");
console.log(user.name);
console.log(user);


/**

Задание 3.
Расширить функционал объекта из предыдущего задания:
Добавить метод, который добавляет объекту новое свойство с указанным
значением.
Продвинутая сложность:
Метод должен быть «умным» — он генерирует ошибку при создании нового свойства
свойство с таким именем уже существует. */