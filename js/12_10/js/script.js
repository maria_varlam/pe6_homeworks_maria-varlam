"use strict";

// const p = document.querySelector("p");
// const input = document.querySelector("input");

// p.addEventListener("keydown", function(e) { // может работать только благодаря contenteditable=""
//   console.log(e);
// });

// input.addEventListener("keydown", function(e) {
//   console.log(e);
// });

// document.addEventListener("keydown", function(e) {
//   console.log(e);
// });

// window.addEventListener("keydown", function(e) {
//   console.log(e);
// });

/**
Задание 1.
Создать элемент h1 с текстом «Нажмите любую клавишу.».
При нажатии любой клавиши клавиатуры менять текст элемента h1 на:
«Нажатая клавиша: ИМЯ_КЛАВИШИ». */

// const root = document.querySelector("#root");
// const h1 = document.createElement("h1");
// h1.textContent = "Enter any keynote";
// root.append(h1);

// function h1Handler(e) {
//   h1.textContent = `Нажатая клавиша:${e.key}`;
// }

// document.addEventListener("keydown", h1Handler);
// document.addEventListener("keydown", function(e) {
//   h1.textContent = `Нажатая клавиша:${e.key}`;
// });

/**

Задание 2.
При вводе текта в инпут "на лету" выводить вводимый текст в параграф ниже».
*/
// const root = document.querySelector("#root");
// const input = document.querySelector("input");
// const par = document.createElement("p");

// input.after(par);

// const inputHandler = e => {
//   if (e.data) {
//     par.textContent += e.data;
//   }
// };

// input.addEventListener("input", inputHandler);

////// Advanced
/**

Задача 3 (событие прокрутки)

На странице с марджинов 234vh расположен абзац текста. Прокрутка страницы
доступна. При прокрутке страницы, когда весь параграф появится на странице
вывести в алерт сообщение "Теперь вам все видно!". Под параграфом расстояние до
конца страницы не ограничено.*/

// const p = document.querySelector("p");
// document.addEventListener("scroll", function(e) {
//   const pRect = p.getBoundingClientRect();
//   if (pRect.bottom <= window.innerHeight && pRect.bottom >= pRect.height) {
//     alert("Теперь вам все видно!");
//   }
// });

/**
Задача 4 (события клавиатуры)
На странице расположено текстовое поле textarea. Создать функцию, которая
ограничивает ввод данных, запрещая вводить цифры. При попытке пользователя
ввести цыфры выводить под полем текст ошибки: "Вводить можно только буквы и
символы!"*/
/**
Задача 5 (события клавиатуры)
На странице расположен input. Создать функцию, которая отслеживает вводимые
данные и запрещает вводить заглавные буквы. При вводе больших букв выводить
сообщение "Заглавные буквы вводить запрещено!".*/
/**
Задача 6 (события клавиатуры) На странице расположено текстовое поле textarea.
Изначально на странице ничего не происходит. Когда польователь начинает вводить
данные в поле (поле переходит в статус фокуса) начинает работ функция
userTracking. Когда пользователь останавливает или прекращает ввод данных (поле
теряет фокус больше чем на 2 секунды) функция выводит сообщение: "Заполните
поле!". Условием "заполненого" поля считается наличие 20 символов в значении
поля.*/

