"use strict";

// for (let i = 0; i < 10; i++) {
//   console.log(i);
// }
// for (let i = 0; i < 10; i++) {
//     if (i === 5){
//         // break;
//         // continue
//     }
//     console.log(i);
//   }

// do {
//     console.log('do while');
// } while (false);

// while(false){
//     console.log('while');
// }

/**

Задание 1.
С помощью цикла вывести в консоль первые все нечётные числа,
которые находятся в диапазоне от 0 до 300.
Заметка:
Чётное число — это число, которое делится на два.
Нечётное число — это число, которое не делится на два.
Продвинутая сложность:
Не выводить в консоль числа, которые делятся на 5. */

// for (let number = 0; number <= 300; ++number) {
//   if (number % 2 !== 0 && number % 5 !== 0) {
//     console.log(number);
//   }
// }

/**

Задание 2.
Пользователь должен ввести два числа.
Если введённое значение не является числом,
программа продолжает опрашивать пользователя до тех пор,
пока он не введёт число.
Когда пользователь введёт два числа, вывести в консоль сообщение:
«Поздравляем. Введённые вами числа: «ПЕРВОЕЧИСЛО» и «ВТОРОЕЧИСЛО».». */

// let numb1 = prompt("Please enter 1st number");
// let numb2 = prompt("Please enter 2nd number");

// while (
//   !numb1 ||
//   isNaN(+numb1) ||
//   typeof +numb1 !== "number" ||
//   !numb2 ||
//   isNaN(+numb2) ||
//   typeof +numb1 !== "number"
// ) {
//   numb1 = prompt("Please enter 1st number");
//   numb2 = prompt("Please enter 2nd number");
// }
// console.log("Поздравляем. Введённые вами числа: ${numb1} и ${numb1}.");
/**

Задание 3.
Написать программу, которая будет опрашивать и приветствовать пользователя.
Программа должна узнать у пользователя его:
Имя;
Фамилию;
Год рождения.
Если пользователь вводит некорректные данные,
программа должна повторно опрашивать его до тех пор,
пока данные не будут введены корректно.
Данные считается введёнными некорректно, если:
Пользователь не вводит в поле никаких данных;
Год рождения, введённый пользователем меньше, чем 1910 или больше, чем
текущий год.
Когда пользователь введёт все необходимые данные корректно,
вывести в консоль сообщение:
«Добро пожаловать, родившийся в ГОД_РОЖДЕНИЯ, ИМЯ ФАМИЛИЯ.». */

// let userName = prompt("Please enter your name");
// let userSurname = prompt("Please enter your Surname");
// let userYear = prompt("Please enter your year of birth");

// while (
//   userName === "" ||
//   userSurname === "" ||
//   +userYear < 1910 ||
//   +userYear > 2021
// ) {
//   userName = prompt("Please enter your name");
//   userSurname = prompt("Please enter your Surname");
//   userYear = prompt("Please enter your year of birth");
// }
// console.log(
//   `Добро пожаловать, родившийся в ${userYear}, ${userName} ${userSurname}.`
// );

// let userName;
// let userSurname;
// let userYear;
// do{
//     userName = prompt("Please enter your name");
//     userSurname = prompt("Please enter your Surname");
//     userYear = prompt("Please enter your year of birth");
//   }
// while (
//   userName === "" ||
//   userSurname === "" ||
//   +userYear < 1910 ||
//   +userYear > 2021
// )
// console.log(
//   `Добро пожаловать, родившийся в ${userYear}, ${userName} ${userSurname}.`
// );
/**

Задание 4.
Написать программу-помощник преподавателя.
Будем использовать американскую систему оценивания знаний.
Эта система работает на баллах и оценках в виде букв.
Расшифровывается следующим образом:
Баллы | Оценка |
95-100 | A |
90-94 | A- |
85-89 | B+ |
80-84 | B |
75-79 | B- |
70-74 | C+ |
65-69 | C |
60-64 | C- |
55-59 | D+ |
50-54 | D |
25-49 | E |
0-24 | F |
Программа должна спрашивать имя и фамилию студента, а также количество
баллов, которое на набрал.
Программа должна повторно запрашивать данные, если были некорректно введены:
Имя студента (строка, состоящая минимум из двух слов);
Количество баллов, которое набрал студент (число от 0 до 100).
Если все данные данные введены верно, программа конвертирует
числовое количество баллов в буквенную оценку и выводит сообщение в консоль:
«К студенту ИМЯ_СТУДЕНТА прикреплена оценка «ОЦЕНКА».».
После выведения этого сообщения программа должна спросить,
есть-ли необходимость сконвертировать оценку для ещё одного студента,
и должна начинать свою работу сначала до тех пор, пока пользователь не
ответит «Нет.».
Когда пользователь откажется продолжать работу программы, программа выводит
сообщение:
«✅ Работа завершена.». */
// let flag = true;
// while (flag){
// let studentName;
// let studentScore;

// do {
//   studentName = prompt("Enter your Name and Surname");
//   studentScore = prompt("Enter your score");
// } while (
//   !studentScore ||
//   studentScore < 0 ||
//   studentScore > 100 ||
//   !studentName
// );
// if (studentScore>=95){
//     studentScore="A";
// } else if (studentScore>=90 && studentScore<=94 ) {
//     studentScore="A-";
// }else if (studentScore>=80 && studentScore<=84) {
//     studentScore="B+";
// }else if (studentScore>=85 && studentScore<=89) {
//     studentScore="B";
// }else if (studentScore>70 && studentScore<=74) {
//     studentScore="C+";
// }else ielse if (studentScore>=64 && studentScore<=68) {
//     studentScore="C";
// }else if (studentScore>=60 && studentScore<=64) {
//     studentScore="C-";
// }else if (studentScore>=55 && studentScore<=59) {
//     studentScore="D+";
// }else if (studentScore>=50 && studentScore<=54) {
//     studentScore="D";
// }else if (studentScore>=25 && studentScore<=49) {
//     studentScore="E";
// }else{
//     studentScore="F";
// }
// console.log(`К студенту ${studentName} прикреплена оценка ${studentScore}.`);
// if(!confirm("Do you want to continue?")) {
//     flag = !flag;
//     console.log("Work is over!);
// } }

/**

Задание 5.
Написать программу-калькулятор.
Программа запрашивает у пользователя три значения:
Первое число;
Второе число;
Математическая операция, которую необходимо совершить над введёнными
числами.
Программа должна повторно запрашивать данные, если:
Любое необходимых чисел не соответствуют критерию корректного числа;
Математическая операция не является одной из: +, -, /, *, **.
Если все данные введены верно, программа вычисляет указанную операцию, и
выводит в консоль результат:
«Над числами ЧИСЛО_1 и ЧИСЛО_2 была произведена операция ОПЕРАЦИЯ. Результат:
РЕЗУЛЬТАТ.`.
После первого успешного выполнения программа должна спросить, есть-ли
необходимость выполниться ещё раз,
и должна начинать свою работу сначала до тех пор, пока пользователь не
ответит «Нет.».
Когда пользователь откажется продолжать работу программы, программа выводит
сообщение:
«✅ Работа завершена.». */
let flag = true;
while (flag) {
let num1 = promt("Enter first number");
let num2 = promt("Enter second number");
let operation = prompt("Enter +,-,*,/,**");
let res;

if (operation!==""){
    switch(operation){
        case:"+":
        res=num1+num2
        break;
        case:"-":
        res=num1-num2
        break;
        case:"*":
        res=num1*num2
        break;
        case:"/":
        res=num1/num2
        break;
        case:"**":
        res=num1**num2
        break;
    }
    console.log(`Над числами ${num1} и ${num2} была произведена операция ${operation}. Результат:
${res}`);
} if (confirm("Do you want to continue?")) {
    flag = !flag;
}
}
console.log("Work is over!");