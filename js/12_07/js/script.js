"use strict";

// const btn = document.querySelector("button");
// console.log(btn);

// btn.addEventListener("click", function() {
//   alert("Hi all");
// });

// btn.addEventListener("click", function() {
//   console.log("Hi"); //this вместо Hi будет ссылаться на элемент
// });

// btn.addEventListener("click", function(event) {
//   console.log(event); //можно вызвать свойства события
// });

// btn.addEventListener("click", function(event) {
//   console.log(event.target === this); //цель события совпадает с элементом на которм произошло событие
// });

// btn.addEventListener("click", btnClickerHandler);
// function btnClickerHandler (event){ // используем либо такое название
//   console.log(event.target === this);
//   btn.removeEventListener("click", btnClickerHandler); //событе нужно удалитьь из памяти
// }
// function onBtnClick (event){//либо такое
//   console.log(event.target === this);
// }

/**
##Задание 1.
Написать скрипт, который создаст элемент button с текстом «Войти». При клике по
кнопке выводить alert с сообщением: «Доtбро пожаловать!».
Callback — это функция, которая срабатывает в ответ на событие. Совсем как в
сервисом с функцией «перезвоните мне». Мы оставляем заявку «перезвоните мне» —
это событие. Затем специалист на перезванивает (calls back).*/

// const root = document.querySelector("#root");
// const buttonCreator = function() {
//   const button = document.createElement("button");
//   button.textContent = "Enter";
//   button.addEventListener("click", () => {
//     alert("Welcome");
//   });
//   root.append(button);
// };
// buttonCreator();

// /**
// ##Задание 2.
// Улучшить скрипт из предыдущего задания. При наведении на кнопку указателем мыши,
// выводить alert с сообщением: «При клике по кнопке вы войдёте в систему.».
// Сообщение должно выводиться один раз.
// Условия:
// Решить задачу грамотно.

// const root = document.querySelector("#root");

// const buttonCreator = function() {
//   const button = document.createElement("button");
//   button.textContent = "Enter";
//   root.append(button);
//   button.addEventListener("click", () => {
//     alert("Welcome");
//   });
//   function buttonEnter() {
//     alert("При клике по кнопке вы войдёте в систему.");
//     button.removeEventListener("mouseenter", buttonEnter);
//   }
//   button.addEventListener("mouseenter", buttonEnter);
// };
// buttonCreator();

// /**
// ##Задание 3.
// Создать элемент h1 с текстом «Добро пожаловать!». Под элементом h1 добавить
// элемент button c текстом «Раскрасить». При клике по кнопке менять цвет каждой
// буквы элемента h1 на случайный.
// /_ Дано _/ const PHRASE = 'Добро пожаловать!';
// function getRandomColor() { const r = Math.floor(Math.random() _ 255); const g =
// Math.floor(Math.random() _ 255); const b = Math.floor(Math.random() * 255);
// return rgb(${r}, ${g}, ${b}); }
// /**

const PHRASE = "Добро пожаловать!";
function getRandomColor() {
  const r = Math.floor(Math.random() * 255);
  const g = Math.floor(Math.random() * 255);
  const b = Math.floor(Math.random() * 255);
  return `rgb(${r}, ${g}, ${b}`;
}

function createDubleTag() {
  const root = document.querySelector("#root");
  const createHeader = document.createElement("h1");
  createHeader.textContent = "Добро пожаловать!";
  root.append(createHeader);
  const button = document.createElement("button");
  button.textContent = "Раскрасить";
  createHeader.after(button);
  const changeColor = function() {
    const inH = createHeader.textContent;
    let arrStr = inH.split("");
    const array = arrStr.map(element => {
      return `<span style="color:${getRandomColor()}">${element}</span>`;
    });
    // console.log(array);
    createHeader.innerHTML = array.join("");
  };
  button.addEventListener("click", changeColor);
}
createDubleTag();
// ##Задание 4.
// Улучшить скрипт из предыдущего задания. При каждом движении курсора по странице
// менять цвет каждой буквы элемента h1 на случайный.
// /_ Дано _/ const PHRASE = 'Добро пожаловать!';
// function getRandomColor() { const r = Math.floor(Math.random() _ 255); const g =
// Math.floor(Math.random() _ 255); const b = Math.floor(Math.random() * 255);
// return rgb(${r}, ${g}, ${b}); }
// /**
// ##Задание 5.
// <style>
// .block {
// width: 400px;
// height: 250px;
// margin: 50px;
// border: 1px solid #000;
// }
// </style>
// div class="block" id="block-1">Block 1</div>
// <div class="block" id="block-2">Block 2</div>
// При наведенні на блок 1 робити блок 2 зеленим кольором. А при наведенні на блок
// 2 робити блок 1 червоним кольором.
// /**
// ##Задание 6.
// <input type="text" id="input">
// <button id="validate-btn">Validate</button>
// При натисканні на кнопку Validate відображати

// VALID зеленим кольром, якщо значення проходить валідацію
// INVALID червоним кольором, якщо значення не проходить валідацію

// Правила валідації значення:

// значення не пусте

// ADVANCED

// Правила валідації значення:

// повинно містити щонайменше 5 символів

// не повинно містити пробілів

// повинно починатися з літери (потрібно використати регулярні вирази)

// /**
// ##Задание 7.
// <button class="counter">0</button>

// Початкове значення кнопки повинно дорівнювати 0
// При натисканні на кнопку збільшувати це значення на

// /**
// ##Задание 8.
// <p id="counter">Counter: 0</p>
// <button id="decrement-btn">-</button>
// <button id="increment-btn">+</button>

// Початкове значення лічильника 0
// При натисканні на + збільшувати лічильник на 1
// При натисканні на - зменшувати лічильник на 1

// ADVANCED: не давати можливості задавати лічильник менше 0

// /**
// ##Задание 9.
// <div>
// <input type="text" id="new-good-input">
// <button id="add-btn">Add</button>
// </div>
// <ul class="shopping-list">

// </ul>
