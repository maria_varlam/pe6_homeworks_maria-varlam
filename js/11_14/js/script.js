"use strict";

// if (condition){
//     действие которое выполняется по условию (to do)
// }

// const age = 17;
// if (age === 18) {
//   console.log("Hi");
// } else if (age < 18) {
//   console.log("Bye");
// } else {
//   console.log("Hmm");
// }
// Можно писать отдельными if, разницы нет

// const variant = "B";
// switch(переменная значений которую будем проверять){
// кейсы "А": console.log ('Your choise is ${variant} (A)');
// }

// switch (variant) {
//   case "А":
//     console.log("Your choise is ${variant} (A)");
//     break;
//   case "B":
//     console.log("Your choise is ${variant} (B)");
//     break;
//   case "C":
//     console.log("Your choise is ${variant} (C)");
//     break;
//   case "D":
//     console.log("Your choise is ${variant} (D)");
//     break;
//   default:
//     console.log("Variant not find");
// }

// const name = "Uasya";

// let age;
// if (name === "Uasya") {
//   age = 18;
// } else {
//   age = 0;
// }
// То что вверху можно записать через тернарный оператор, который ниже:
// const age = name==="Uasya" ? 18:0;

/**

Задание 1.
Пользователь вводит в модальное окно любое число.
В консоль вывести сообщение:
Если число чётное, вывести в консоль сообщение «Ваше число чётное.»;
Если число не чётное, вывести в консоль сообщение «Ваше число не чётное.»;
Если пользователь ввёл не число, вывести новое модальное окно с сообщением
«Необходимо ввести число!».
Если пользователь во второй раз ввёл не число, вывести сообщение: «⛔️
Ошибка! Вы ввели не число.». */

/**
Задание 2.
Написать программу, которая будет приветствовать пользователя.
Сперва пользователь вводит своё имя, после чего программа выводит в консоль
сообщение с учётом его должности.
Список должностей:
Mike — CEO;
Jane — CTO;
Walter — программист:
Oliver — менеджер;
John — уборщик.
Если введёно не известное программе имя — вывести в консоль сообщение
«Пользователь не найден.».
Выполнить задачу в двух вариантах:
используя конструкцию if/else if/else;
используя конструкцию switch. */

// let name = prompt("Enter your name");
// if (name==="Mike"){
//     console.log("Hello, Mike CEO");
// } else if (name==="Jane") {
//     console.log("Hello, Jane CTO");
// } else if (name==="Walt"er) {
//     console.log("Hello, Walter программист");
// } else if (name==="Oliv"er) {
//     console.log("Hello, Oliver менеджер");
// }else if (name==="John") {
//     console.log("Hello, John уборщик");
// } else {
//     console.log("Пользователь не найден.");
// }

// switch(name){
//     case "Mike":console.log("Hello, Mike CEO");
//     break;
//     case "Jane":console.log("Hello, Jane CTO");
//     break;
//     case "Walter":console.log("Hello, Walter программист");
//     break;
//     case "Oliver":console.log("Hello, Oliver менеджер");
//     break;
//     case "John":console.log("Hello, John уборщик");
//     break;
//     default:
//         console.log("Пользователь не найден.");
// }

/**

Задание 3.
Пользователь вводит 3 числа.
Вывести в консоль сообщение с максимальным числом из введённых.
Если одно из введённых пользователем чисел не является числом,
вывести сообщение: «⛔️ Ошибка! Одно из введённых чисел не является числом.».
Условия: объектом Math пользоваться нельзя. */

// let numb1 = prompt("Please, enter 1st number");
// let numb2 = prompt("Please, enter 2nd number");
// let numb3 = prompt("Please, enter 3rd number");

// if (
//   numb1 &&
//   !isNaN(+numb1) &&
//   typeof +numb1 === "number" &&
//   numb2 &&
//   !isNaN(+numb2) &&
//   typeof +numb1 === "number" &&
//   numb3 &&
//   !isNaN(+numb3) &&
//   typeof +numb1 === "number"
// ) {
//   if (+numb1 > +numb2 && +numb1 > +numb3) {
//     console.log(numb1);
//   } else if (+numb2 > +numb1 && +numb2 > +numb3) {
//     console.log(numb2);
//   } else {
//     console.log(numb3);
//   }
// } else {
//   console.log("⛔️ Ошибка! Одно из введённых чисел не является числом.");
// }

/**

Задание 4.
Напишите программу «Кофейная машина».
Программа принимает монеты и готовит напитки:
Кофе за 25 монет;
Капучино за 50 монет;
Чай за 10 монет.
Чтобы программа узнала что делать, она должна знать:
Сколько монет пользователь внёс;
Какой он желает напиток.
В зависимости от того, какой напиток выбрал пользователь,
программа должна вычислить сдачу и вывести сообщение в консоль:
«Ваш «НАЗВАНИЕ НАПИТКА» готов. Возьмите сдачу: «СУММА СДАЧИ».".
Если пользователь ввёл сумму без сдачи — вывести сообщение:
«Ваш «НАЗВАНИЕ НАПИТКА» готов. Спасибо за сумму без сдачи! :)" */

//

// let product = prompt("Ente a product");
// let coins = prompt("Enter a number of coins");
// const COFFE_COST = 25;
// const CAP_COST = 50;
// const TEA_COST = 10;

// let rest;
// switch (product) {
//   case "coffee":
//     rest = +cois - COFFE_COST;
//     break;
//   case "cappucino":
//     rest = +cois - CAP_COST;
//     break;
//   case "tea":
//     rest = +cois - TEA_COST;
//     break;
// }
// if (rest === 0) {
//   console.log("Ваш " + product + " готов. Спасибо за сумму без сдачи! :)");
// } else if (rest < 0) {
//   console.log("Not enough money");
// } else {
//   console.log("Ваш " + product + " готов. Возьмите сдачу: " + rest);
// }

/**
Задача 5.
const adminStatus = false; const adminName:"Uasya", "Petro", "Max", "Olha";
Польвозватель сайта вводит свое имя. Если имя естьв списке админов, изменить
статус пользователя (adminStatus) на true; */

// const adminStatus = false;
// const adminName = prompt("Enter your name");

// switch (adminStatus) {
//   case "Uasya":
//     adminStatus = true;
//     break;
//   case "Petro":
//     adminStatus = true;
//     break;
//   case "Max":
//     adminStatus = true;
//     break;
//   case "Olha":
//     adminStatus = true;
//     break;
//   default:
//     break;
// }
// const adminStatus = false;
// const adminName = prompt("Enter your name");
// if (
//     adminName === "Uasya"
// )

