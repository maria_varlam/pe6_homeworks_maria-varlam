"use strict";

// Функции внутри обЪекта называются методами
// Объект струтктурный тип данныъх, внутри записываем свойства

// const myAge =0;
// const myAgeTitle ="my age";
// const obj = {
//     name:"Mari",
//     "birth year":1990,
//     myAge: myAge,
// [myAgeTitle]: myAge,
// getAge: function(){
//     return obj["my age"];
// }
// };
// // Получить значение свойства:\
// console.log(obj.name);
// console.log(obj.["birth year",]);
// console.log(obj.getAge()); //так вызываем функцию

// Для копирования объектов: (перебрали ключи для первого объекта и копировали его для пустого объекта 2)
// const obj2 = {};
// for (const key in obj) {
//     obj2[key]=obj[key];
// }
// for in просто перебирает свойства

// Копирование объектов: (объединяет несколько объектов-в нашем случае пустой объект и первый)
// const obj3 = Object.assign({}, obj);
// console.log(obj3)
// значения при этом записываются из последнего - в нашем случае с obj

// instanceof -возвращает true/false - проверяет наследственност об]ектов -- проверяем то что слева (null) на то что справа-
// console.log(null instanceof Object)

// const obj4 = {
//     set name(value){
//         this._name=value;
//     }
//         get name(){
//             return this._name;
//         }
// }
// obj4.name ="Uasya"; //тут срабатывает Setter, потому что =
// console.log(obj4);
// console.log(obj4.name); //тут getter

const obj5 = {
  a: 4,
  b: "string",
  c: undefined
};
// Проверка на наличие свойства у объекта - возвращает true/false - толлько одно свойство проверяем
console.log("a" in obj5);
console.log(obj5.hasOwnproperty("a"));
console.log(Object.hasOwn(obj5, "a")); //этот новее и лучше использовать его

console.log(Object.keys(obj5)); //возвращает свойства объекта в формате массива - в нашем случае a,b,c
console.log(Object.values(obj5)); //возвращает значения объекта в формате массива - в нашем случае 4, string, undefined
