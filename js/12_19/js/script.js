"use strict";
// const body = document.body;
// const list = document.getElementById("list");

// const listItem = [...document.querySelectorAll(".list-item")];

// function itemClickHandler(e) {
//   console.log("element click", e.target);
// }
// listItem.forEach(el => {
//   el.addEventListener("click", itemClickHandler);
// });

// list.addEventListener("click", function itemClickHandler(e) {
//   if (e.target.classList.contains("list-item")) {
//     //тоже самое что и в 10 стороке перебирали forEach
//     console.log("list click", e.target);
//   }
// });
// body.addEventListener("click", function itemClickHandler(e) {
//   console.log("body click", e.target);
// });
//   body.addEventListener("click", function itemClickHandler(e) {
//     console.log("body click", e.target);
//   },
//   true); -если добавить третий элемент true то можно отследить весь процесс нажатия (погружение), без true всплытие

/**
Задача 1.
Необходимо «оживить» навигационное меню с помощью JavaScript.
При клике на элемент меню добавлять к нему CSS-класс .active.
Если такой класс уже существует на другом элементе меню, необходимо
с того, предыдущего элемента CSS-класс .active снять.
У каждый элемент меню — это ссылка, ведущая на google.
С помощью JavaScript необходимо предотвратить переход по всем ссылка на этот
внешний ресурс.
Условия:
В реализации обязательно использовать приём делегирования событий (на
весь скрипт слушатель должен быть один).
       <ul id="menu">
       <li><a href="https://google.com">PHP</a>
       <ul>
       <li><a href="https://google.com">Справочник</a></li>
       <li><a href="https://google.com">Сниппеты</a></li>
       </ul>
       </li>
       <li><a href="https://google.com">HTML</a>
       <ul>
       <li><a href="https://google.com">Информация</a></li>
       <li><a href="https://google.com">Примеры</a></li>
       </ul>
       </li>
       </ul>
*/
// const list = document.querySelector("#menu");

// const menuClickHandler = e => {
//   e.preventDefault();

//   const activeItem = document.querySelector(".active");
//   const item = e.target.parentElement;

//   if (activeItem) {
//     activeItem.classList.remove("active");
//   }
//   item.className = "active";
// };
// list.addEventListener("click", menuClickHandler);

/**
Задача 2.
Есть список сообщений. Добавьте каждому сообщению по кнопке для его скрытия.
Картинка для кнопки удаления
<style>
.close-btn {
background-color: transparent;
border: none;
cursor: pointer;
position: absolute;
right: 0;
top: 0;
width: 30px;
height: 30px;
}
.pane{
padding-left: 30px;
position: relative;
}
</style>

<ul id="messages">
<li class="pane">
<h3>Лошадь</h3>
<p>Домашняя лошадь — животное семейства непарнокопытных, одомашненный и единственный сохранившийся подвид дикой лошади, вымершей в дикой природе, за исключением небольшой популяции лошади Пржевальского.</p>
<button type="button" aria-label="close" class="close-btn">
<svg width="32px" height="32px">
<use href="#close">
</svg>
</button>
</li>
<li class="pane">
<h3>Осёл</h3>
<p>Домашний осёл или ишак — одомашненный подвид дикого осла, сыгравший важную историческую роль в развитии хозяйства и культуры человека. Все одомашненные ослы относятся к африканским ослам.</p>
<button type="button" aria-label="close" class="close-btn">
<svg width="32px" height="32px">
<use href="#close">
</svg>
</button>
</li>
<li class="pane">
<h3>Корова, а также пара слов о диком быке, о волах и о тёлках. </h3>
<p>Коро́ва — самка домашнего быка, одомашненного подвида дикого быка, парнокопытного жвачного животного семейства полорогих. Самцы вида называются быками, молодняк — телятами, кастрированные самцы — волами. Молодых (до первой стельности) самок называют тёлками.</p>
<button type="button" aria-label="close" class="close-btn">
<svg width="32px" height="32px">
<use href="#close">
</svg>
</button>
</li>
</ul>
<div  style="display: none;">
<svg viewBox="0 0 32 32" id="close">
<title>close</title>
<path
clip-rule="evenodd"
d="M16,0C7.163,0,0,7.163,0,16c0,8.836,7.163,16,16,16   c8.836,0,16-7.163,16-16C32,7.163,24.836,0,16,0z M16,30C8.268,30,2,23.732,2,16C2,8.268,8.268,2,16,2s14,6.268,14,14   C30,23.732,23.732,30,16,30z"
fill="#121313"
fill-rule="evenodd"
/>
<path
clip-rule="evenodd"
d="M22.729,21.271l-5.268-5.269l5.238-5.195   c0.395-0.391,0.395-1.024,0-1.414c-0.394-0.39-1.034-0.39-1.428,0l-5.231,5.188l-5.309-5.31c-0.394-0.396-1.034-0.396-1.428,0   c-0.394,0.395-0.394,1.037,0,1.432l5.301,5.302l-5.331,5.287c-0.394,0.391-0.394,1.024,0,1.414c0.394,0.391,1.034,0.391,1.429,0   l5.324-5.28l5.276,5.276c0.394,0.396,1.034,0.396,1.428,0C23.123,22.308,23.123,21.667,22.729,21.271z"
fill="#121313"
fill-rule="evenodd"
/>
</svg></div>
*/
// const list = document.querySelector("#messages");
// function listClickHandler(e) {
//   const item = e.target;
//   if (item.closest(".close-btn")) {
//     item.closest(".pane").remove();
//   }
// }
// list.addEventListener("click", listClickHandler);

// const timer = setTimeout(() => {
//     console.log("Done")
//     clearTimeout(timer)
// }, 1000);

// const interval = setInterval(()=>{
//     console.log("interval")
//     clearInterval(interval)//останавливает интервал
// }, 1000)

// localStorage.setItem("key", "value");
// localStorage.setItem("num", "33");

/**
Задание 1.
Написать программу для напоминаний.
Все модальные окна реализовать через alert.
Условия:
Если пользователь не ввёл сообщение для напоминания — вывести alert с сообщением
«Ведите текст напоминания.»;
Если пользователь не ввёл значение секунд,через сколько нужно вывести
напоминание — вывести alert с сообщением «Время задержки должно быть больше
одной секунды.»;
Если все данные введены верно, при клике по кнопке «Напомнить» необходимо её
блокировать так, чтобы повторный клик стал возможен после полного завершения
текущего напоминания; После этого вернуть изначальные значения обоих полей;
Создавать напоминание, если внутри одного из двух элементов input нажать клавишу
Enter; После загрузки страницы установить фокус в текстовый input.

  <section>
  <h1>Напоминалка</h1>
  <div>
  <label for="reminder">Напомнить</label>
  <input type="text" value="" placeholder="Напомнить..." id="reminder" />
  </div>
  <div>
  <label for="seconds">Через</label>
  <input type="number" value="0" id="seconds" />
  <label for="seconds">секунд</label>
  </div>
  <button>Напомнить!</button>
  </section>
*/

// const form = document.querySelector("#form");
// const remind = document.querySelector("#reminder");
// const seconds = document.querySelector("#seconds");
// const btn = document.querySelector("#btn");
// remind.focus();
// function formClickHandler(e) {
//   e.preventDefault();

//   if (!remind.value) {
//     alert("Ведите текст напоминания.");
//   } else if (seconds.value <= 1) {
//     alert("Время задержки должно быть больше одной секунды.");
//   } else {
//     btn.disabled = true;
//     let timer = setTimeout(function() {
//       alert(remind.value);
//       clearTimeout(timer);
//       remind.value = "";
//       seconds.value = "";
//       btn.disabled = false;
//     }, seconds.value * 1000);
//   }
// }
// form.addEventListener("submit", formClickHandler);

/**
 При загрузке страницы посмотреть - есть ли в localStorage значение по ключу
userName? Если есть - выводить сообщение "Hello, userName" на экран, где вместо
userName должно быть вставлено значение по одноименному ключу в localStorage
Если значения по такому ключу нет - спросить у пользователя имя, модальным
окном, записать его в localStorage. После этого вывести сообщение "Hello,
userName" на экран,где вместо userName должно быть вставлено значение по
одноименному ключу в localStorage */

// if (localStorage.getItem("userName")) {
//   alert(`Hello, ${localStorage.getItem("userName")}`);
// } else {
//   localStorage.setItem("userName", JSON.stringify(prompt("Please enter name")));
// }
/**
 Сделать форму логина с сохранением данных в localStorage. 
 При загрузке страницы посмотреть - есть ли в localStorage значение по ключу
userName? Если есть - выводить сообщение "Hello, userName" на экран, где вместо
userName должно быть вставлено значение по одноименному ключу в localStorage
Если значения по такому ключу нет - спросить у пользователя имя, модальным
окном, записать его в localStorage. После этого вывести сообщение "Hello,
userName" на экран, где вместо userName должно быть вставлено значение по
одноименному ключу в localStorage */

const form = document.createElement("form");
const inputName = document.createElement("input");
const inputPass = document.createElement("input");
const button = document.createElement("button");
form.append(inputName, inputPass, button);
inputPass.type = "password";
button.textContent = "Save";
document.body.append(form);
form.addEventListener("submit", function() {
  localStorage.setItem("login", JSON.stringify(inputName.value));
  localStorage.setItem("pass", JSON.stringify(inputPass.value));
});

// localStorage.setItem("userName");
